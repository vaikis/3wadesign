<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('design.index');
});

Route::get('home', function () {
    return view('home.index');
});

Route::get('test', function() {
	return view ('home.index');
});

Route::resource('clothes', 'ClotheController');


Route::get('/summer', 'SummerController@index')->name('summer.index');

Route::get('/autumn', function(){
	return view('autumn.index');
})->name('autumn.index');


Auth::routes();
Auth::routes();

Route::get('/home', 'HomeController@index');
