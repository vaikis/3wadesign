@extends('main')
@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
  <title>VK Design 3WA</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>
<body>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="http://www.ibcworldnews.com/gallery/wp-content/uploads/2016/01/047ba164-8693-4028-8815-c419f090fa14.jpg" alt="Image" width="300" height="300">
        <div class="carousel-caption">
          <h3>Clothing for special women!</h3>
          <p>Get exclusive design today!</p>
        </div>      
      </div>

      <div class="item">
        <img src="http://kingofwallpapers.com/fashion-images/fashion-images-020.jpg" alt="Image" width="300" height="300">
      </div>

      <div class="item">
        <img src="https://s-media-cache-ak0.pinimg.com/originals/21/4d/de/214ddee5f850843957e1281abee0ee2a.jpg?text=New summer 2017 Collection" alt="Image" width="300" height="300">
        <div class="carousel-caption">
          <h3>More Sell $</h3>
          <p>Lorem ipsum...</p>
        </div>      
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
  
<div class="container text-center">    
  <h3>What We Do</h3><br>
  <div class="row">
    <div class="col-sm-4">
      <img src="http://www.tucsonmodelgroup.com/wp-content/uploads/2016/05/tucson-model-agency.jpg?text=Collection" class="img-responsive" style="width:100%" alt="Image">
      <a href="{{ route('summer.index')}}">Summer collection</a>
    </div>
    <div class="col-sm-4"> 
      <img src="http://www.uricom.nl/wp-content/uploads/spring-fascion3-1.jpg?text=IMAGE" class="img-responsive" style="width:100%" alt="Image">
      <a href="{{ route('autumn.index')}}">Autumn 2017 Collection</a>    
    </div>
    <div class="col-sm-4">
      <div class="well">
       <p>Some text..</p>
      </div>
      <div class="well">
       <p>Some text..</p>
      </div>
    </div>
  </div>
</div><br>


<div class="container">
	<div class="header clearfix">
		
		<h3 class="text-muted">

		</h3>
	</div>
</div>
<div class="jumbotron container">
	<p class="lead"></p>

	<img class="container image-responsive" src="http://www.nandos.com/sites/all/themes/nandos/images/restaurants/restaurant-carousel-1.jpg">
</div>


  

@endsection