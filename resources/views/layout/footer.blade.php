<footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <div class="logofooter"> Logo</div>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
        <p><i class="fa fa-map-pin"></i> Gedimino ave. 32-3 Vilnius Lithuania</p>
        <p><i class="fa fa-phone"></i> Phone (Lithuania) : +37060660012</p>
        <p><i class="fa fa-envelope"></i> E-mail : info@design.lt</p>
      
       </div>
          <div class="col-md-6 col-sm-6">
            <h5 class="white-text">Nuorodos</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="{{ URL::route('clothes.index') }}">Clothes</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">About us</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"></a></li>
            </ul>
            </div>
        
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        © 2016 Design Company
        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
      </div>
    </div>
  </div>
</div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('js/cart.js')}}"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>

</body>
</html>