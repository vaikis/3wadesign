@extends('main')
@section('content')

@if(isset($clothe))
{!!Form::model($clothe, ['route' => ['clothes.update', $clothe->id], 'method'=>'PUT'] )!!}
@else
{!!Form::open(['route' => 'clothes.store', 'method'=>'POST'] )!!}
@endif

{{csrf_field()}}

{!! Form::label('title', 'Title :')!!}
{!! Form::text('title', null, ['class'=>'form-control'] )!!}

{!! Form::label('description', 'Description:')!!}
{!! Form::text('description', null,  ['class'=>'form-control'] )!!}

{!! Form::label('photo', 'Photo :')!!}
{!! Form::text('photo', null, ['class'=>'form-control'] )!!}

{!! Form::label('size', 'Size:')!!}
{!! Form::text('size', null, ['class'=>'form-control'] )!!}


{!! Form::label('price', 'Price :')!!}
{!! Form::number('price', null, ['class'=>'form-control'] )!!}

{!! Form::label('quantity', 'Quantity :')!!}
{!! Form::number('quantity', null, ['class'=>'form-control'] )!!}





{!!Form::submit('Submit')!!}
{!! Form::close() !!}



@if(isset($clothe))

	{{ Form::open(['route' => ['clothes.destroy', $clothe->id], "method" => "POST"]) }}
		<input type="hidden" name="_method" value="DELETE">

		{{ csrf_field() }}

	
		{!! Form::submit('Detele', ['class' => 'btn btn-danger']); !!}
	{!! Form::close() !!}

@endif

@endsection



