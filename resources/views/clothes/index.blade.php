@extends('main')
@section('content')

<div class="container">
	<div class="row">
				@foreach($clothes as $clothe)
		<div class="col-md-3 ">
			<ul>

				<li>
					<h3>{{ $clothe->title }}</h3>
					<small>{{ $clothe->price }}</small>

					<p>{{ mb_strimwidth($clothe->description, 0 , 50, '...') }}</p>

					<p>{{ $clothe->photo }}</p>

					<a href="{{ route('clothes.edit', $clothe->id)}}" class="btn btn-info">EDIT</a>
					<a href="{{ route('clothes.create')}}" class="btn btn-info">Add Clothes </a>
					<a href=" {{ route('clothes.show', $clothe->id) }} " class="btn btn-primary">READ MORE</a>
				</li>
			</ul>
		</div>

		@endforeach
	</div>
</div>
@endsection