<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    protected $fillable = [
    'title',
    'size',
    'price',
    'image',
    'description',
    ];
}
