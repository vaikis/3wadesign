<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClotheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clothes=\App\Clothe::all();
        return view('clothes.index', compact('clothes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clothes = \App\Clothe::all();
        return view('clothes.create', compact('clothes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        \App\Clothe::create($request->all());
        return redirect()->route('clothes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $clothe = \App\Clothe::find($id);
        return view('clothes.show', compact('clothe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $clothe = \App\Clothe::find($id);

    $clothes = \App\Clothe::all(); 
    $selectClothes = array();
    foreach ($clothes as $clothe) {
    $selectMenus[$clothe->id] = $clothe->name;
    }

    return view("clothes.create", compact("clothe", "selectClothe"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $clothe = \App\Clothe::find($id);
        $clothe->update($request->all());

        return redirect()->route('clothes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          
        \App\Clothe::find($id)->delete();
       

        return redirect()->route('clothes.index');
    
    }
}
