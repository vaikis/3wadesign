<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clothe extends Model
{
    //
    protected $fillable = [
    'title',
    'description',
    'photo',
    'size',
    'price',
    'quantity',
    ];
}
